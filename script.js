/*
  Реализовать функцию для подсчета n-го обобщенного числа Фибоначчи. 
  Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

Технические требования:

-Написать функцию для подсчета n-го обобщенного числа Фибоначчи. 
Аргументами на вход будут три числа - F0, F1, n, где F0, F1 - первые два числа последовательности 
(могут быть любыми целыми числами), n - порядковый номер числа Фибоначчи, которое надо найти. 
Последовательнось будет строиться по следующему правилу F2 = F0 + F1, F3 = F1 + F2 и так далее.
-Считать с помощью модального окна браузера число, которое введет пользователь (n).
-С помощью функции посчитать n-е число в обобщенной последовательности Фибоначчи и вывести его на экран.
-Пользователь может ввести отрицательное число - результат надо посчитать по такому же правилу (F-1 = F-3 + F-2).
*/

/* https://www.dcode.fr/fibonacci-numbers */
"use strict";
const isNumber = (string) =>
  (typeof string === "string" && string.trim().length) ||
  typeof string === "number"
    ? !isNaN(Number(string))
    : false;

const getNumbersWithPrompt = () => {
  let f0 = "";
  let f1 = "";
  let n = "";
  do {
    f0 = prompt("Enter F0 number of fibonnaci sequense", f0);
    if (f0 === null) return null;
    f1 = prompt("Enter F1 number of fibonnaci sequense", f1);
    if (f1 === null) return null;
    n = prompt("Enter N number of fibonnaci sequense", n);
    if (n === null) return null;
  } while (!isNumber(f0) || !isNumber(f1) || !isNumber(n));
  return [parseInt(f0), parseInt(f1), parseInt(n)];
};

const fibSeq = [];
const fib = (f0, f1, n) => {
  //fibSeq.push(f1);
  if (n === 1 || n === -1) {
    return f1;
  }
  return fib(f1, n > 0 ? f0 + f1 : f0 - f1, n > 0 ? --n : ++n);
};

// alternative syntax
// const fib = (f0, f1, n) =>
//   n === 1 || n === -1
//     ? f1
//     : fib2(f1, n > 0 ? f0 + f1 : f0 - f1, n > 0 ? --n : ++n);

const fibCalculator = () => {
  const promptResult = getNumbersWithPrompt();
  if (promptResult === null) return;
  console.log(fib(...promptResult));
  // console.log(fibSeq);
};
fibCalculator();
// const testCases = () => {
//   console.log(fib(0, 1, 10));
//   console.log(fib(1, 1, 10));
//   console.log(fib(1, 2, 10));
//   console.log(fib(10, 20, 10));
//   console.log(fib(10, 20, -10));
//   console.log(fib(0, 1, -30));
//   console.log(fib(1, 1, -30));
//   console.log(fib(1, 2, -30));
// };
//testCases();
